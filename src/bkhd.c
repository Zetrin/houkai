#include "akpk.h"
#include "bkhd.h"
#include "didx.h"
#include "hirc.h"
#include "riff.h"
#include "io.h"

int bkhd_read(reader *r, const char *parent)
{
	bkhd_header bkhd;
	const auto base = reader_tell(r);


	read_bytes(r, &bkhd, sizeof(bkhd));
	reader_seek(r, base + bkhd.size + 8);
	
	auto magic = read_int32le(r);
	reader_seek(r, base + bkhd.size + 8);

	switch (magic) {
		case AKPK:
			akpk_read(r, parent);
			break;
		case BKHD:
			bkhd_read(r, parent);
			break;
		case DIDX:
			didx_read(r, parent);
			break;
		case HIRC:
			hirc_read(r, nullptr);
			break;
		case RIFF:
			//wem_info(io.data, io.size);
			riff_read(r, parent);
			break;
		default:
			fprintf(stderr, "Unknown chunk: %X\n", magic);
	}

	return 0;
}

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
