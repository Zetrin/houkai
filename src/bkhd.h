#ifndef BKHD_H
#define BKHD_H

#include <unistd.h>
#include <inttypes.h>
#include "io.h"

typedef struct bkhd_header {
	uint32_t magic;
	uint32_t size;
	uint32_t version;
	uint32_t soundbank_id;
	uint32_t bank_id;
	uint32_t _reserved;
	uint32_t revision;
} bkhd_header;

int bkhd_read(reader *r, const char *parent);
#endif

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
