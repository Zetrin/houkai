#define _GNU_SOURCE
#include <fcntl.h>
#include <limits.h>
#include <memory.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <immintrin.h>
#include <nmmintrin.h>
#include <xmmintrin.h>

#include "riff.h"
#include "io.h"


#define WAVE 0x45564157
#define FMT0 0x20746D66

/* http://soundfile.sapp.org/doc/WaveFormat */

typedef enum chunk_type {
	/*RIFF_FMT	= 0x666d7420,*/
	RIFF_FMT	= 0x20746d66,
	RIFF_WAVE	= 0x45564157,
	RIFF_DATA	= 0x61746164,
	RIFF_LIST	= 0x5453494C,
	RIFF_CUE	= 0x20657563,
	RIFF_JUNK	= 0x4B4E554A,
	RIFF_AKD	= 0x20646B61,
	RIFF_LPMS	= 0x6C706D73,
} riff_chunk_type;

typedef enum codec_t { CODEC_PCM, CODEC_WAVE } codec_t;

typedef struct riff_chunk {
	uint32_t chunk_id;
	uint32_t size;
	uint32_t type;
} riff_chunk;

typedef struct fmt_chunk {
	uint32_t chunk_id;
	uint32_t size;
	uint16_t format_tag;
	uint16_t channels;
	uint32_t sample_rate;
	uint32_t avg_byte_rate;
	uint16_t block_size;
	uint16_t bits_per_sample;
} fmt_chunk;

typedef struct fmt_extra_chunk {
	uint32_t extra_size;
	union {
		uint16_t valid_bits_per_sample;
		uint16_t samples_per_block;
		uint16_t _reserved;
	} channel_layout;
	uint32_t channel_mask;
	struct {
		uint32_t data1;
		uint16_t data2;
		uint16_t data3;
		uint32_t data4;
		uint32_t data5;
	} guid;
} fmt_extra_chunk;

typedef struct data_chunk {
	uint32_t chunk_id;
	uint32_t size;
} data_chunk;

typedef struct cue_chunk {
	uint32_t chunk_id;
	uint32_t size;
	uint32_t points;
} cue_chunk;

typedef struct cue_point {
	uint32_t chunk_id;
	uint32_t position;
	uint32_t fcc_chunk;
	uint32_t chunk_start;
	uint32_t block_start;
	uint32_t offset;
} cue_point;

typedef struct list_chunk {
	uint32_t chunk_id;
	uint32_t size;
	uint32_t adtl;
} list_chunk;

typedef struct labl {
	uint32_t chunk_id;
	uint32_t size;
	uint32_t cue_point_id;
} labl;

typedef struct junk_chunk {
	uint32_t chunk_id;
	uint32_t size;
	/* uint8_t data[size]*/
} junk_chunk;

typedef struct lpms_chunk {
	uint32_t chunk_id;
	uint32_t size;
	uint32_t manufacturer;
	uint32_t product;
	uint32_t sample_period;
	uint32_t midi_unity_note;
	uint32_t midi_pitch_fraction;
	uint32_t smpte_format;
	uint32_t smpte_offset;
} lpms_chunk;


static uint32_t adler32(const uint8_t *buf, size_t len);

/*
void wem_info(void *wem_data, size_t size)
{
	uint32_t chunk;
	riff_chunk *riff;
	void *pos = wem_data;

	riff = (struct riff_chunk *)pos;

	if (riff->chunk_id != RIFF) {
		printf("Error while reading WEM: RIFF expected.\n");
		return;
	}

	pos = (void *)((uintptr_t)pos + sizeof(*riff));

	while (pos < (void *)((uintptr_t)wem_data + riff->size)) {
		chunk = *(uint32_t *)pos;
		fmt_chunk *fmt;
		data_chunk *data;
		cue_chunk *cue;
		list_chunk *list;
		junk_chunk *junk;
		lpms_chunk *lpms;

		switch (chunk) {
			case RIFF_FMT:
				fmt = (fmt_chunk *)pos;
				pos = OFFSET(pos, fmt->size + 4 + 4);
				break;
			case RIFF_DATA:
				data = (data_chunk *)pos;
				pos = OFFSET(pos, 4 + 4 + data->size);
				break;
			case RIFF_CUE:
				cue = (cue_chunk *)pos;
				pos = OFFSET(pos, 4 + 4 + cue->size);
				// cue points wanna see?
				break;
			case RIFF_LIST:
				list = (list_chunk *)pos;
				pos = OFFSET(pos, 4 + 4 + list->size);
				break;
			case RIFF_AKD:
			case RIFF_JUNK:
				junk = (junk_chunk *)pos;
				pos = OFFSET(pos, 4 + 4 + junk->size);
				break;
			case RIFF_LPMS:
				lpms = (lpms_chunk *)pos;
				pos = OFFSET(pos, 4 + 4 + lpms->size);
				break;
			default:
				printf("Unknown chunk: %X\n", chunk);
				pos = OFFSET(pos, 4);
				break;
		}
	}
}*/


static bool save_wem(void *data, size_t size, const char *path)
{
	writer w;

	if (path) {
		//wem_info(data, size);
		if (writer_set_write_file(&w, path)) {
			write_bytes(&w, data, size);
			writer_release(&w);
			return false;
		}
	}

	return false;
}


bool riff_read(reader *r, const char *parent)
{
	const auto base = reader_get_data(r);
	const auto offset = reader_tell(r);
	riff_chunk riff;
	char path[PATH_MAX] = {0};
	uint32_t adler;


	read_bytes(r, &riff, sizeof(riff));
	reader_seek(r, offset);
	adler = adler32(base, riff.size + 8);

	if (parent) {
		make_path(parent);
		snprintf(path, sizeof(path), "%s/%u.wem", parent, adler);
	} else
		snprintf(path, sizeof(path), "%u.wem", adler);

	printf("\t%s\n", path);

	reader_skip(r, riff.size + 8);

	return save_wem(base, riff.size + 8, path);
}


static uint32_t adler32(const uint8_t *buf, size_t len)
{
	const uint32_t BASE = 65521U;
	const uint32_t NMAX = 5552;
	const unsigned BLOCK_SIZE = 32;
	uint32_t s1 = 1;
	uint32_t s2 = 0;

	size_t blocks = len / BLOCK_SIZE;
	len -= blocks * BLOCK_SIZE;

	while (blocks) {
		unsigned n = NMAX / BLOCK_SIZE;
		if (n > blocks) {
			n = (unsigned) blocks;
		}
		blocks -= n;
		const __m128i tap1 = _mm_setr_epi8(32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17);
		const __m128i tap2 = _mm_setr_epi8(16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
		const __m128i zero = _mm_setr_epi8(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		const __m128i ones = _mm_set_epi16(1, 1, 1, 1, 1, 1, 1, 1);

		__m128i v_ps = _mm_set_epi32(0, 0, 0, s1 * n);
		__m128i v_s2 = _mm_set_epi32(0, 0, 0, s2);
		__m128i v_s1 = _mm_set_epi32(0, 0, 0, 0);
		do {
			const __m128i bytes1 = _mm_loadu_si128((__m128i*)(buf));
			const __m128i bytes2 = _mm_loadu_si128((__m128i*)(buf + 16));

			v_ps = _mm_add_epi32(v_ps, v_s1);
			v_s1 = _mm_add_epi32(v_s1, _mm_sad_epu8(bytes1, zero));
			const __m128i mad1 = _mm_maddubs_epi16(bytes1, tap1);
			v_s2 = _mm_add_epi32(v_s2, _mm_madd_epi16(mad1, ones));
			v_s1 = _mm_add_epi32(v_s1, _mm_sad_epu8(bytes2, zero));
			const __m128i mad2 = _mm_maddubs_epi16(bytes2, tap2);
			v_s2 = _mm_add_epi32(v_s2, _mm_madd_epi16(mad2, ones));
			buf += BLOCK_SIZE;
		} while (--n);
		v_s2 = _mm_add_epi32(v_s2, _mm_slli_epi32(v_ps, 5));

#define S23O1 _MM_SHUFFLE(2,3,0,1)
#define S1O32 _MM_SHUFFLE(1,0,3,2)
		v_s1 = _mm_add_epi32(v_s1, _mm_shuffle_epi32(v_s1, S23O1));
		v_s1 = _mm_add_epi32(v_s1, _mm_shuffle_epi32(v_s1, S1O32));
		s1 += _mm_cvtsi128_si32(v_s1);
		v_s2 = _mm_add_epi32(v_s2, _mm_shuffle_epi32(v_s2, S23O1));
		v_s2 = _mm_add_epi32(v_s2, _mm_shuffle_epi32(v_s2, S1O32));
		s2 = _mm_cvtsi128_si32(v_s2);
#undef S23O1
#undef S1O32
		s1 %= BASE;
		s2 %= BASE;
	}

	if (len) {
		if (len >= 16) {
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			s2 += (s1 += *buf++);
			len -= 16;
		}
		while (len--) {
			s2 += (s1 += *buf++);
		}
		if (s1 >= BASE) {
			s1 -= BASE;
		}
		s2 %= BASE;
	}

	return s1 | (s2 << 16);
}
// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
