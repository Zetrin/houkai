#include "didx.h"
#include "akpk.h"
#include "io.h"
#include "riff.h"


int didx_read(reader *r, const char *parent)
{
	didx_header didx;
	didx_data data;
	unsigned int count;
	didx_entry *entries;


	read_bytes(r, &didx, sizeof(didx));

	auto data_offset = reader_tell(r) + didx.size;

	auto num = didx.size / sizeof(didx_entry);

	entries = reader_get_data(r);

	reader_seek(r, data_offset);
	
	read_bytes(r, &data, sizeof(data));

	if (data.magic != DATA)
		return -1;

	data_offset = reader_tell(r);

	for (auto i = 0; i < num; i++) {
		reader_seek(r, data_offset + entries[i].data_offset);

		auto curr = reader_tell(r);
		auto magic = read_int32le(r);
		reader_seek(r, curr);

		switch (magic) {
			case RIFF:
				riff_read(r, parent);
				break;
			default:
				fprintf(stderr, "Unknown soundbank container type: %X\n", magic);
		}
	}

	return 0;
}

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
