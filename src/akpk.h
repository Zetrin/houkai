#ifndef AKPK_H
#define AKPK_H

#include <uchar.h>
#define _XOPEN_SOURCE 700

#include <inttypes.h>
#include "io.h"

typedef enum section_type {
	AKPK = 0x4B504B41,
	BKHD = 0x44484B42,
	HIRC = 0x43524948,
	DIDX = 0x58444944,
	DATA = 0x41544144,
	RIFF = 0x46464952,
	INIT = 0x54494E49,
} section_type;

typedef struct akpk_header {
	uint32_t magic;
	uint32_t size;
	uint32_t version;
	uint32_t lang_map_size;
	uint32_t sb_lut_size;
	uint32_t stm_lut_size;
	uint32_t ext_lut_size;
} akpk_header;

/*typedef struct sb_entry32 {
	uint32_t id;
	uint32_t block_size;
	uint32_t file_size;
	uint32_t start_block;
	uint32_t language_id;
} soundbank_entry32;*/

/*typedef struct sb_entry64 {
	uint64_t id;
	uint32_t block_size;
	uint32_t file_size;
	uint32_t start_block;
	uint32_t language_id;
} soundbank_entry64;*/

typedef struct sb_entry {
	uint64_t id;
	uint32_t size;
	uint32_t offset;
	uint32_t language;
	char *name;
} soundbank;

typedef struct lang_entry {
	uint32_t offset;
	uint32_t id;
} language_entry;

typedef struct language {
	uint32_t id;
	char name[64];
} language;

typedef struct lang_map {
	uint32_t count;
	struct language *map;
} language_map;

typedef struct wem {
	uint64_t id;
	uint32_t offset;
	uint32_t size;
	uint32_t language_id;
	bool to_extract;
} wem_entry;

typedef struct wem_list {
	uint32_t count;
	struct wem *items;
	uint32_t _allocated;
} wem_list;

typedef struct chunk_info {
	uint32_t magic;
	uint32_t size;
} chunk_info;

typedef struct sb_table {
	uint32_t count;
	struct sb_entry *map;
} soundbank_table;

typedef struct akpk_pck {
	struct akpk_header header;
	struct lang_map languages;
	struct sb_table soundbank;
	struct sb_table statement;
	struct sb_table externals;
} akpk_pck;

typedef enum sb_type {
	SB_SIZE_32 = 0,
	SB_SIZE_64,
} sb_type_t;

bool akpk_read(reader *r, const char *parent);
#endif

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
