#ifndef DIDX_H
#define DIDX_H

#include <stdint.h>
#include "io.h"

typedef struct didx_header {
	uint32_t magic;
	uint32_t size;
} didx_header;

typedef struct __attribute__((packed)) didx_entry {
	uint32_t wem_id;
	uint32_t data_offset;
	uint32_t size;
} didx_entry;

typedef struct didx_data {
	uint32_t magic;
	uint32_t size;
} didx_data;

int didx_read(reader *r, const char *parent);
#endif

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
