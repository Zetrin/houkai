#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <threads.h>
#include <memory.h>
#include <limits.h>
#include <strings.h>
#include <uchar.h>
#include "akpk.h"
#include "bkhd.h"
#include "hirc.h"
#include "io.h"
#include "riff.h"


typedef struct thread_info {
	thrd_t thid;
	int index;
	int result;
	akpk_pck *pck;
	wem_list *list;
	int cpus;
} thread_info;


static char *get_language_str(language_map map, uint32_t id)
{
	for (auto i = 0; i < map.count; i++) {
		if (map.map[i].id == id)
			return map.map[i].name;
	}

	return nullptr;
}


static size_t char16to8(const char16_t *restrict src, char *restrict dst, size_t max_size)
{
	mbstate_t state;
	auto sz = 0;
	char16_t *in = (char16_t*)src;
	char *p;


	if (src == nullptr)
		return -1;

	while(*(in++)) {}

	sz = in - src;
	p = dst;

	for (auto n = 0; n < sz && n < max_size; ++n)
		p += c16rtomb(p, src[n], &state);

	return sz;
}


static bool read_language_map(reader *r, size_t size, language_map *languages)
{
	const auto ending = reader_tell(r) + size;
	const auto base = r->state.data + r->state.offset;
	const auto count = read_int32le(r);


	languages->map = calloc(count, sizeof(*languages->map));
	if (languages->map == nullptr) {
		perror("Failed to allocate memory for language map");
		return false;
	}
	languages->count = count;

	for (auto i = 0; i < count; i++) {
		auto offset = read_int32le(r);
		char16_t *str = (char16_t*)(base + offset);

		languages->map[i].id = read_int32le(r);
		char16to8(str, languages->map[i].name, sizeof(languages->map[i].name));
	}

	reader_seek(r, ending);

	return true;
}


static bool read_soundbank_map(reader *r, size_t size, soundbank_table *table, language_map languages, sb_type_t type)
{
	const auto ending = reader_tell(r) + size;
	const auto count = read_int32le(r);


	table->map = calloc(count, sizeof(*table->map));
	if (table->map == nullptr) {
		perror("Could not allocate memory for 32-bit entry map");
		return false;
	}
	table->count = count;

	switch (type) {
		case SB_SIZE_32:
			for (auto i = 0; i < count; i++) {	
				table->map[i].id = read_int32le(r);
				auto block_size = read_int32le(r);
				auto file_size = read_int32le(r);

				table->map[i].size = block_size * file_size;
				table->map[i].offset = read_int32le(r);
				table->map[i].language = read_int32le(r);
				table->map[i].name = get_language_str(languages, table->map[i].language);
			}
			break;

		case SB_SIZE_64:
			for (auto i = 0; i < count; i++) {
				table->map[i].id = read_int64le(r);
				auto block_size = read_int32le(r);
				auto file_size = read_int32le(r);

				table->map[i].size = block_size * file_size;
				table->map[i].offset = read_int32le(r);
				table->map[i].language = read_int32le(r);
				table->map[i].name = get_language_str(languages, table->map[i].language);
			}
			break;

		default:
			return false;
	}

	reader_seek(r, ending);

	return true;
}


static bool parse_soundbank(reader *r, soundbank_table sb, const char *parent)
{
	chunk_info info;
	char path[PATH_MAX];
	uint32_t adler;


	for (auto i = 0; i < sb.count; i++) {
		reader_seek(r, sb.map[i].offset);
		read_bytes(r, &info, sizeof(info));
		reader_seek(r, sb.map[i].offset);
		
		memset(path, 0, sizeof(path));

		switch (info.magic) {
			case AKPK:
				akpk_read(r, parent);
				break;
			case BKHD:
				bkhd_read(r, parent);
				break;
			case HIRC:
				hirc_read(r, nullptr);
				break;
			case RIFF:
				snprintf(path, sizeof(path), "%s/%s", parent, sb.map[i].name);
				riff_read(r, path);
				break;
			default:
				fprintf(stderr, "Unknown soundbank container type: %X\n", info.magic);
				break;
		}
		reader_seek(r, sb.map[i].offset + sb.map[i].size);
	}

	return true;
}


static void akpk_free(const akpk_pck *pck)
{
	if (pck->languages.count)
		free(pck->languages.map);

	if (pck->soundbank.count)
		free(pck->soundbank.map);

	if (pck->statement.count)
		free(pck->statement.map);

	if (pck->externals.count)
		free(pck->externals.map);
}


bool akpk_read(reader *r, const char *parent)
{
	akpk_pck pck = {0};
	char path[PATH_MAX] = {0};


	read_bytes(r, &pck.header, sizeof(pck.header));

	if (pck.header.version != 1) {
		fprintf(stderr, "Supported version 1 but file version is %u\n", pck.header.version);
		return false;
	}

	if (pck.header.lang_map_size > 4) {
		if (!read_language_map(r, pck.header.lang_map_size, &pck.languages))
			return false;
	} else {
		perror("Missing language map");
		return false;
	}

	if (pck.header.sb_lut_size > 4)
		read_soundbank_map(r, pck.header.sb_lut_size, &pck.soundbank, pck.languages, SB_SIZE_32);
	else
		reader_skip(r, sizeof(pck.header.sb_lut_size));

	if (pck.header.stm_lut_size > 4)
		read_soundbank_map(r, pck.header.stm_lut_size, &pck.statement, pck.languages, SB_SIZE_32);
	else
		reader_skip(r, sizeof(pck.header.stm_lut_size));

	if (pck.header.ext_lut_size > 4)
		read_soundbank_map(r, pck.header.ext_lut_size, &pck.externals, pck.languages, SB_SIZE_64);
	else
		reader_skip(r, sizeof(pck.header.ext_lut_size));

	if (parent)
		strlcat(path, parent, sizeof(path) -1);

	parse_soundbank(r, pck.soundbank, path);
	parse_soundbank(r, pck.statement, path);
	parse_soundbank(r, pck.externals, path);

	akpk_free(&pck);

	return true;
}
// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
