#include "hirc.h"
#define _GNU_SOURCE
#include <fcntl.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "akpk.h"
#include "bkhd.h"
#include "didx.h"
#include "riff.h"
#include "io.h"


static char *my_basename(char *basename, size_t maxlen, const char *restrict path)
{
	char *p = (char *)path;
	char *filename = (char *)path;
	auto len = 0;


	while (*p != '\0') {
		if (*p == '/' || *p == '\\')
			filename = p + 1;

		p++;
	}

	p = filename;
	while (*p != '\0') {
		if (*p == '.')
			break;

		len++;
		p++;
	}

	memcpy(basename, filename, len);
	basename[len] = '\0';

	return basename;
}


int main(int argc, const char *argv[])
{
	char path[PATH_MAX];
	reader r;


	if (argc != 2) {
		fprintf(stderr, "Filename required.\n");
		return EXIT_FAILURE;
	}

	reader_set_read_file(&r, argv[1]);

	my_basename(path, sizeof(path), argv[1]);

	while (r.state.offset < r.state.size) {
		auto start = reader_tell(&r);
		auto magic = read_int32le(&r);
		reader_seek(&r, start);

		switch (magic) {
			case AKPK:
				akpk_read(&r, path);
				break;
			case BKHD:
				bkhd_read(&r, path);
				break;
			case DIDX:
				didx_read(&r, path);
				break;
			case HIRC:
				hirc_read(&r, nullptr);
				break;
			case RIFF:
				//wem_info(io.data, io.size);
				riff_read(&r, path);
				break;
			default:
				fprintf(stderr, "Unknown chunk.\n");
				magic = read_int32le(&r);
				goto out;
		}
	}

out:
	reader_release(&r);

	return EXIT_SUCCESS;
}

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
