#ifndef IO_H
#define IO_H

#include <fcntl.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>


#if (__GNUC__ >= 3)
# define unlikely(cond) __builtin_expect ((cond), 0)
# define likely(cond) __builtin_expect ((cond), 1)
#else
# define unlikely(cond) (cond)
# define likely(cond) (cond)
#endif


typedef struct io reader;
typedef struct io writer;
typedef struct io_state reader_state;
typedef struct io_state writer_state;

typedef void* (*read_fn)(reader *r, size_t size);
typedef int (*write_fn)(writer *w, const void *buffer, size_t size);
typedef int (*align_fn)(struct io *x, size_t align);
typedef off_t (*seek_fn)(struct io *x, off_t offset);

struct io_state {
	uint8_t *data;
	int fd;
	size_t size;
	size_t offset;
	void (*free)(struct io_state *io);
};

struct io {
	struct io_state state;
	read_fn read;
	write_fn write;
	align_fn align;
	seek_fn seek;
};

size_t align(off_t offset, unsigned n);

void reader_free(reader *r);
void writer_free(writer *w);
void reader_release(reader *r);
void writer_release(writer *w);

void reader_set_read_memory(reader *r, void *data, size_t size);
bool reader_set_read_file(reader *r, const char *path);
void writer_set_write_memory(writer *w, void **data);
bool writer_set_write_file(writer *w, const char *path);

bool reader_align(reader *r, size_t align);
bool writer_align(writer *w, size_t align);
off_t reader_seek(reader *r, off_t seek);
off_t writer_seek(reader *r, off_t seek);

uint64_t reader_tell(reader *r);
uint64_t writer_tell(writer *w);

void reader_skip(reader *r, uint32_t length);
void *reader_get_data(reader *r);

uint8_t read_byte(reader *r);
void read_bytes(reader *r, void *data, size_t size);
uint16_t read_int16be(reader *r);
uint16_t read_int16le(reader *r);
uint32_t read_int32be(reader *r);
uint32_t read_int32le(reader *r);
uint64_t read_int64be(reader *r);
uint64_t read_int64le(reader *r);
void read_string(reader *r, char *dst, size_t maxlen);
ssize_t reader_strlen(reader *r);

void write_byte(writer *w, uint8_t value);
void write_bytes(writer *w, const void *data, size_t size);
void write_int16be(writer *w, uint16_t value);
void write_int16le(writer *w, uint16_t value);
void write_int32be(writer *w, uint32_t value);
void write_int32le(writer *w, uint32_t value);
void write_int64be(writer *w, uint64_t value);
void write_int64le(writer *w, uint64_t value);
void write_string(writer *w, char *str);

int make_path(const char *const path);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
