#define _GNU_SOURCE
#include <assert.h>
#include <endian.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <unistd.h>
#include "io.h"


uint64_t reader_tell(reader *r) __attribute__ ((alias ("tell")));
uint64_t writer_tell(writer *w) __attribute__ ((alias ("tell")));
off_t reader_seek(reader *r, off_t offset) __attribute__ ((alias ("seek")));
off_t writer_seek(reader *r, off_t offset) __attribute__ ((alias ("seek")));
void reader_free(reader *r) __attribute__ ((alias ("io_free")));
void writer_free(writer *w) __attribute__ ((alias ("io_free")));
void reader_release(reader *r) __attribute__ ((alias ("io_release")));
void writer_release(writer *w) __attribute__ ((alias ("io_release")));


#ifdef __linux__
int make_path(const char *const path)
{
	char buf[PATH_MAX];
	const size_t len = strlen(path);
	char *a;

	memcpy(&buf, path, len + 1);

	a = buf;
	do {
		if (a == &buf[0] && *a == '/') {
			++a;
			continue;
		}

		if (*a == '/') {
			*a = '\0';
			if(mkdir(buf, S_IRWXU) < 0) {
				if (errno != EEXIST) {
					fprintf(stderr, "Can not create directory %s: %s\n", buf, strerror(errno));
					return -1;
				}
			}
			*a = '/';
			++a;
			continue;
		}

		if (*a == '\0') {
			if(mkdir(buf, S_IRWXU) < 0) {
				if (errno != EEXIST) {
					fprintf(stderr, "Can not create directory %s: %s\n", buf, strerror(errno));
					return -1;
				}
			}
			break;
		}

		++a;
	} while(1);

	return 0;
}


static void close_file(struct io_state *s)
{
	if (likely(ftruncate(s->fd, s->size) == 0)) {
		fsync(s->fd);
		close(s->fd);
	} else {
		perror("Error: ftruncate failed");
	}
}


static void mmap_close_file(struct io_state *s)
{
	munmap(s->data, s->size);
}


static bool mmap_open_read(const char *path, struct io_state *s)
{
	int fd;
	struct stat st;
	void *addr;
	bool ret = false;


	fd = open(path, O_RDONLY | O_NOFOLLOW);
	if (unlikely(fd == -1)) {
		perror("Error: can not open file");
		return false;
	}

	if (unlikely(fstat(fd, &st) == -1)) {
		perror("Error: can not stat file");
		goto out;
	}

	addr = mmap(nullptr, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (unlikely(addr == MAP_FAILED)) {
		perror("Error: can not mmap file");
		goto out;
	}

	s->data = addr;
	s->size = st.st_size;
	s->offset = 0;
	s->free = mmap_close_file;

	ret = true;
out:
	close(fd);
	return ret;
}


static int write_file(writer *w, const void *buffer, size_t size)
{
	writer_state *s = &w->state;
	size_t check;

	check = pwrite(s->fd, buffer, size, s->offset);
	if (unlikely(check != size)) {
		fprintf(stderr, "Error: can not write bundle data: %s\n", strerror(errno));
		return -1;
	} else {
		s->offset += size;
		s->size += size;
		return s->offset;
	}
}


static bool file_open_write(const char *path, struct io_state *s)
{
	int fd;
	struct stat st;


	fd = open(path, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
	if (unlikely(fd == -1)) {
		perror("Error: can not open file to write");
		return false;
	}

	if (unlikely(fstat(fd, &st) == -1)) {
		perror("Error: can not stat file");
		return false;
	}

	s->fd = fd;
	s->size = st.st_size;
	s->offset = 0;
	s->data = nullptr;
	s->free = close_file;

	return true;
}
#endif


size_t align(off_t offset, unsigned n)
{
	return ((offset + (n - 1)) & ~(n - 1)) - offset;
}


void reader_skip(reader *r, uint32_t length)
{
	reader_seek(r, r->state.offset + length);
}


off_t seek(struct io *x, off_t offset)
{
	return x->seek(x, offset);
}


off_t seek_common(struct io *x, off_t offset)
{
	if (unlikely(offset < 0))
		offset = 0;

	if (unlikely(x->state.size < (size_t)offset))
		offset = x->state.size - 1;

	x->state.offset = offset;

	return x->state.offset;
}


off_t seek_output_mem(writer *w, off_t offset)
{
	writer_state *s = &w->state;
	void *tmp;

	if (unlikely(offset < 0)) {
		w->state.offset += offset;
		return w->state.offset;
	}

	if (likely(s->offset + offset > s->size)) {
		tmp = realloc(s->data, s->offset + offset);
		if (likely(tmp != nullptr)) {
			memset(tmp + s->size, 0, offset - s->size);
			s->size = offset;
			s->offset = offset;
			s->data = tmp;
		} else {
			perror("Error: can not seek to memory");
			return -1;
		}
	} else {
		s->offset = offset;
	}

	return s->offset;
}


void *reader_get_data(reader *r)
{
	return (void *)(r->state.data + r->state.offset);
}


uint64_t tell(struct io *x)
{
	return x->state.offset;
}


static int align_output_mem(writer *w, size_t align)
{
	size_t padding;
	writer_state *s = &w->state;

	padding = ((s->offset + (align - 1)) & ~(align - 1)) - s->offset;

	if (unlikely(s->offset + padding > s->size)) {
		return -1;
	} else {
		memset(s->data + s->offset, 0, padding);
		s->offset += padding;
		return s->offset;
	}
}


static int align_input(reader *r, size_t by)
{
	size_t padding;
	reader_state *s = &r->state;

	padding = align(s->offset, by);
	if (unlikely(s-> offset + padding > s->size)) {
		return -1;
	} else {
		s->offset += padding;
		return s->offset;
	}
}


static int align_output_file(writer *w, size_t align)
{
	size_t padding;

	padding = ((w->state.offset + (align - 1)) & ~(align - 1)) - w->state.offset;
	w->state.size += padding;
	// @TODO truncate

	return 0;
}


static int write_memory(writer *w, const void *buffer, size_t size)
{
	writer_state *s = &w->state;
	void *tmp;

	tmp = realloc(s->data, s->size + size);
	if (likely(tmp != nullptr)) {
		s->data = tmp;
		memcpy(s->data + s->offset, buffer, size);
		s->offset += size;
		s->size = s->offset;
		return s->size;
	} else {
		perror("Error: data not written");
		return -1;
	}
}


static void *do_read(reader *r, size_t size)
{
	reader_state *s = &r->state;
	void *data_ptr;

	if (unlikely(s->offset + size > s->size)) {
		fprintf(stderr, "Out of bounds\n");
		return nullptr;
	} else {
		data_ptr = s->data + s->offset;
		s->offset += size;
		return data_ptr;
	}
}


static void unbind_memory(struct io_state *io)
{
	io->data = nullptr;
}


void writer_set_write_to_memory(writer *w, void **out_ptr)
{
	if (out_ptr != nullptr)
		w->state.data = *out_ptr;
	else
		w->state.data = nullptr;
	w->state.offset = 0;
	w->state.size = 0;
	w->state.free = unbind_memory;

	w->read = nullptr;
	w->write = write_memory;
	w->align = align_output_mem;
	w->seek = seek_output_mem;
}


void reader_set_read_memory(reader *r, void *data, size_t size)
{
	r->state.data = data;
	r->state.offset = 0;
	r->state.size = size;
	r->state.free = unbind_memory;

	r->read = do_read;
	r->write = nullptr;
	r->align = align_input;
	r->seek = seek_common;
}


bool reader_set_read_file(reader *r, const char *path)
{
	r->state.data = nullptr;
	r->state.offset = 0;
	r->state.size = 0;
	r->state.free = nullptr;

	r->read = do_read;
	r->write = nullptr;
	r->align = align_input;
	r->seek = seek_common;

	return mmap_open_read(path, &r->state);
}


bool writer_set_write_file(writer *w, const char *path)
{
	w->state.data = nullptr;
	w->state.offset = 0;
	w->state.size = 0;
	w->state.free = close_file;

	w->read = nullptr;
	w->write = write_file;
	w->align = align_output_file;
	w->seek = seek_common;

	return file_open_write(path, &w->state);
}


uint8_t read_byte(reader *r)
{
	uint8_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint8_t)0;

	return *ptr;
}


void write_byte(writer *w, uint8_t value)
{
	w->write(w, &value, sizeof(value));
}


uint16_t read_int16le(reader *r)
{
	uint16_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint16_t)0;

	return *ptr;
}


uint16_t read_int16be(reader *r)
{
	uint16_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint16_t)0;

	return be16toh(*ptr);
}


void write_int16le(writer *w, uint16_t value)
{
	w->write(w, &value, sizeof(value));
}


void write_int16be(writer *w, uint16_t value)
{
	uint16_t val = htobe16(value);
	w->write(w, &val, sizeof(val));
}


uint32_t read_int32le(reader *r)
{
	uint32_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint32_t)0;

	return *ptr;
}


uint32_t read_int32be(reader *r)
{
	uint32_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint32_t)0;

	return be32toh(*ptr);
}


void write_int32le(writer *w, uint32_t value)
{
	w->write(w, &value, sizeof(value));
}


void write_int32be(writer *w, uint32_t value)
{
	uint32_t val = htobe32(value);
	w->write(w, &val, sizeof(val));
}


uint64_t read_int64le(reader *r)
{
	uint64_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint64_t)0;

	return *ptr;
}


uint64_t read_int64be(reader *r)
{
	uint64_t *ptr;

	ptr = r->read(r, sizeof(*ptr));
	if (ptr == nullptr)
		return (uint64_t)0;

	return be64toh(*ptr);
}


void write_int64le(writer *w, uint64_t value)
{
	w->write(w, &value, sizeof(value));
}


void write_int64be(writer *w, uint64_t value)
{
	uint64_t val = htobe64(value);
	w->write(w, &val, sizeof(val));
}


void read_bytes(reader *r, void *data, size_t size)
{
	void *ptr;
	
	ptr = r->read(r, size);
	if (ptr != nullptr)
		memcpy(data, ptr, size);
}


void write_bytes(writer *w, const void *data, size_t size)
{
	w->write(w, data, size);
}


ssize_t reader_strlen(reader *r)
{
	char c;
	ssize_t len = 0;
	int err;
	const uint64_t off = reader_tell(r);

	// @TODO
	do {
		c = read_byte(r);
		++len;
	} while (c != 0);

	reader_seek(r, off);

	return len;
}


void read_string(reader *r, char *dst, size_t maxlen)
{
	char c = 0;
	size_t i = 0;

	do {
		c = read_byte(r);

		if (likely(dst != nullptr))
			dst[i] = c;
		++i;
	} while (c != 0 && i < maxlen);
}


void write_string(writer *w, char *str)
{
	char *src = str;

	do {
		w->write(w, src, 1);
		src++;
	} while (*src != 0);
}


bool reader_align(reader *r, size_t by)
{
	if (likely(r->state.offset + by < r->state.size)) {
		return r->align(r, by);
	} else {
		return false;
	}
}


bool writer_align(writer *w, size_t by)
{
	return w->align(w, by);
}


void io_release(struct io *io)
{
	if (likely(io->state.free != nullptr))
		io->state.free(&io->state);

	io->write = nullptr;
	io->read = nullptr;
	io->align = nullptr;
	io->seek = nullptr;
}


void io_free(struct io *io)
{
	if (likely(io != nullptr)) {
		io_release(io);
		free(io);
	}
}


reader *reader_create_memory_reader(void *data, size_t size)
{
	reader *r;

	r = malloc(sizeof(reader));
	if (unlikely(r == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	reader_set_read_memory(r, data, size);

	return r;
}


reader *reader_create_file_reader(const char *path)
{
	reader *r;

	r = malloc(sizeof(reader));
	if (unlikely(r == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	if (reader_set_read_file(r, path))
		return r;
	else {
		free(r);
		return nullptr;
	}
}


writer *writer_create_memory_writer(void **out)
{
	writer *w;

	w = malloc(sizeof(writer));
	if (unlikely(w == nullptr)) {
		perror("Error: can not allocate memory for writer");
		return nullptr;
	}

	writer_set_write_to_memory(w, out);

	return w;
}


writer *writer_create_file_writer(const char *path)
{
	writer *w;

	w = malloc(sizeof(writer));
	if (unlikely(w == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	if (writer_set_write_file(w, path)) {
		return w;
	} else {
		free(w);
		return nullptr;
	}
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
