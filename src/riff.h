#ifndef RIFF_H
#define RIFF_H

#include <inttypes.h>
#include <stddef.h>
#include "io.h"


void wem_info(void *data, size_t size);
bool riff_read(reader *r, const char *parent);

#endif
// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
