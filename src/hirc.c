#include "hirc.h"
#include "io.h"


void hirc_read(reader *r, const char *path)
{
	hirc_header hirc;

	read_bytes(r, &hirc, sizeof(hirc));

#if 1
	reader_skip(r, hirc.size);
#else
	auto count = read_int32le(r);
	for (auto i = 0; i < count; i++) {
		hirc_object *object = nullptr;

		switch (object->type) {
			case HIRC_SOUND:
				// hirc_obj_snd_t *sound = (hirc_obj_snd_t *)data;
				break;
			case HIRC_MUSIC_TRACK:
				// hirc_obj_mt_t *track = (hirc_obj_mt_t *)data;
				break;
			case HIRC_MOTION_FX:
				// hirc_obj_mfx_t *mfx = (hirc_obj_mfx_t *)data;
				break;
			default:
				fprintf(stderr, "HIRC object type %u is not supported yet. Skip\n", object->type);
				break;
			}
	}
#endif
}

// vim: tabstop=4 shiftwidth=4 noexpandtab smartindent
